<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DFiles extends Model
{
    use HasFactory;
	protected $fillable = [
        'dfilename',
        'dfilesizemb',
		'dfileext',
		'dfileff',
		'dfilemm'
    ];
}

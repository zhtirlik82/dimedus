<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\DFiles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class DFilesController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
		if(isset($_GET['s'])) {
			$s = $_GET['s'];
			$dfiles = DFiles::where('dfilename', 'like', '%' . $s . '%')->get();
			/*$dfiles = DFiles::query()
            ->when(
                $s,
                function (Builder $builder) use ($request) {
                    $builder->where('dfilename', 'like', "%{$s}%")
                        //->orWhere('email', 'like', "%{$s}%");
                }
            )
            ->simplePaginate(5);*/
		} else {
			//$dfiles = DFiles::latest()->get();
			$dfiles = DFiles::paginate(50, ['*'], 'page', $_GET['page']);
		}
        
        return response()->json([
            'success' => true,
            'message' => 'List data post',
            'data' => $dfiles
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }
	
	public function tus()
    {
        //
    }
	
    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
		//$snm = "TUSFILES";
		//$tusfiles = $request->session()->get($snm);
		$TUSSDIR = '/var/www/html/DimedusLaravel/tus/s/';
		$sid = $_GET['sid'];
		$sfn = $TUSSDIR."s".$sid;
		$tusfiles = array();
		if(file_exists($sfn))
			$tusfiles = json_decode(file_get_contents($sfn), true);
		if(isset($tusfiles))
			foreach($tusfiles as $k=>$f) {
				$dfiles = DFiles::create([
					'dfilename' => $f['NM'],
					'dfilesizemb' => $f['SZ'],
					'dfileext' => $f['EX'],
					'dfileff' => $f['FF'],
					'dfilemm' => $f['MM']
				]);
			}
			
		if(file_exists($sfn))
			unlink($sfn);
		
		//$data = $request->session()->all();
		//$sessionId = Session::getId();
		/*
		if(is_array($tusfiles))
		
		$data = $request->session()->all();
		for($i=0; $i<4; $i++) {
			$dfiles = DFiles::create([
				'dfilename' => '1',
				'dfilesizemb' => '1',
				'dfileext' => '1',
				'dfileff' => '1',
				'dfilemm' => '1'
			]);
			$id = $dfiles->id;
			$dfiles->dfilename = $id;
			$dfiles->dfilesizemb = $id;
			$dfiles->dfileext = $id;
			$dfiles->dfileff = $id;
			$dfiles->dfilemm = $id;
			$dfiles->save();
		}
		*/
		/*
        $request->validate([
            //'dfilename' => 'required|string|max:255',
            //'dfilesizemb' => 'required',
			//'dfileext' => 'required|string|max:255'
			'dfilename' => 'max:255',
            'dfilesizemb' => '',
			'dfileext' => 'max:255'
        ]);
		
        $dfiles = DFiles::create([
            'dfilename' => $request->dfilename,
            'dfilesizemb' => $request->dfilesizemb,
			'dfileext' => $request->dfileext
        ]);*/

        return response()->json([
            'success' => true,
            'message' => 'Post created',
            'data' => ''
        ], 201);
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
		if($id=='tus') {
			return response()->json([
				'success' => true,
				'message' => 'Detail data post'.$id,
				'data' => ''
			], 200);
		} else {
			$dFiles = DFiles::find($id);
			return response()->json([
				'success' => true,
				'message' => 'Detail data post'.$id,
				'data' => $dFiles
			], 200);
		}
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(DFiles $dFiles)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'dfilename' => 'required|string|max:255',
            'dfilesizemb' => 'required',
			'dfileext' => 'required|string|max:255'
        ]);
		
		$dFiles = DFiles::find($id);

        if ($dFiles) {
            $dFiles->update([
                'dfilename' => $request->dfilename,
				'dfilesizemb' => $request->dfilesizemb,
				'dfileext' => $request->dfileext
            ]);

            return response()->json([
                'success' => true,
                'message' => 'File updated',
                'data' => $dFiles
            ], 200);
        }
        return response()->json([
            'success' => false,
            'message' => 'id='.$id.' update File not found'
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
		$dFiles = DFiles::find($id);
        if ($dFiles) {
			
            $dFiles->delete();
			
			if(file_exists($dFiles->dfileff)) {
				unlink($dFiles->dfileff);
				/*$server   = new \TusPhp\Tus\Server('file');
				$contents = $server->cache->getCacheContents();
				$server->cache->delete($key);*/
			}
			if(file_exists($dFiles->dfilemm))
				unlink($dFiles->dfilemm);
            return response()->json([
                'success' => true,
                'message' => 'File deleted'
            ], 200);
        }
        return response()->json([
            'success' => false,
            'message' => 'File not found'
        ], 404);
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('d_files', function (Blueprint $table) {
            $table->id();
			$table->string('dfilename');
			//$table->integer('dfilesizemb');
			$table->float('dfilesizemb', 11, 4);
			$table->string('dfileext');
			$table->string('dfileff');
			$table->string('dfilemm');
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('d_files');
    }
};

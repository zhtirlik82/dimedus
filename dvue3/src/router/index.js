import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import DFiles from '../views/dfiles/Index.vue'
import DFileCreate from '../views/dfiles/Create.vue'
import DFileEdit from '../views/dfiles/Edit.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
	{
      path: '/dfiles',
      name: 'dfiles',
      component: DFiles
    },
    {
      path: '/dfiles/create',
      name: 'dfiles.create',
      component: DFileCreate
    },
    {
      path: '/dfiles/:id',
      name: 'dfiles.edit',
      component: DFileEdit
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/AboutView.vue')
    }
  ]
})

export default router

var jJS = {
	atimeout: 10000
	, progressTable
	, arrFiles: []
	, trNames: []
	, arrProgress: []
	, arrProgressBar: []
	, trProgresses: []
	, UploadSuccesses: []
	, uploads: []
	, arrUploadIsRunning: []
	, endpoint1: 'http://tirlik.kz:8001/tus'
	, endpoint: 'https://tirlik.kz/tus/tusep.php'
	, tusurl: "https://tirlik.kz/tus/tusfs.php"
	, _token: ""
	, fileLimitSize: 8*1024*1024
	, fileLimitSize1: 8*1024
	, tmp(ip) {
		
	}
	
	, addFiles(files, _token) {
		this._token = _token;
		var isGreater = false;
		for (var i = 0; i < files.length; i++) {
			if(files[i].size>this.fileLimitSize)
				isGreater = true;
		}
		
		if(isGreater) {
			alert('Размер файла больше '+this.fileLimitSize+' байт');
		} else {
			this.progressTable = document.querySelector('#progressTable')
			for (var i = 0; i < files.length; i++) {		
				this.addFile(files[i]);
			}
			this.sstartUpload()
		}
	}
	
	, addFile(file) {
		var r = 0;
	
		for (var i = 0; i < this.arrFiles.length; i++) {
			var arrfile = this.arrFiles[i];
			if(file.name==arrfile.name) {				
				return r;
			}
		}
		
		
		//this.pauseall();
		//alert(file.name);
		
		
		r = this.arrFiles.length;
		this.arrFiles[r] = file;
		
		//Имя файла
		this.trNames[r] = document.createElement('tr');
		var td = document.createElement('td');
		td.innerHTML = (r+1)+". "+file.name;
		td.style.fontSize = "10px"; 
		this.trNames[r].appendChild(td);
		this.progressTable.appendChild(this.trNames[r]);
		
		//Прогресс
		this.trProgresses[r] = document.createElement('tr');
		td = document.createElement('td');
		this.arrProgressBar[r] = document.createElement('div');
		this.arrProgressBar[r].style.backgroundColor = "#B4A7C3";
		this.arrProgressBar[r].style.fontSize = "10px"; 
		//arrProgressBar[r].innerHTML = "TTTT";
		td.appendChild(this.arrProgressBar[r]);
		this.trProgresses[r].appendChild(td);
		this.progressTable.appendChild(this.trProgresses[r]);
		
		return r;
	}
	
	, sstartUpload: function () {
		if(this.arrFiles.length>0) {
		for (var i = 0; i < this.arrFiles.length; i++) {
		  var file = this.arrFiles[i];
		  this.startUpload(file, i);
		}
		} else {
			alert("Список файлов пустой. Нажмите + для добавления файлов.");
		}
	}
	
	, startUpload: function (file, n) {
	  console.log(file);
	  // Only continue if a file has actually been selected.
	  // IE will trigger a change event even if we reset the input element
	  // using reset() and we do not want to blow up later.
	  if (!file) {
		return
	  }

	  
	  let chunkSize = 100000;
	  if (Number.isNaN(chunkSize)) {
		chunkSize = Infinity
	  }

	  //let parallelUploads = parseInt(parallelInput.value, 10)
	  let parallelUploads = 1;
	  if (Number.isNaN(parallelUploads)) {
		parallelUploads = 1
	  }

	  //toggleBtn.textContent = 'pause upload'
		if(typeof(this.uploads[n]) != "undefined" && this.uploads[n] !== null) {
			if(!this.UploadSuccesses[n]) {
				this.uploads[n].start()
				this.arrUploadIsRunning[n] = true;
			}
		} else {
		  var options = {
			endpoint: jJS.endpoint,
			uploadUrl: jJS.endpoint,
			chunkSize,
			retryDelays: [0, 1000, 3000, 5000],
			parallelUploads,
			metadata   : {
			  filename: file.name,
			  filetype: file.type,
			},
			//document.querySelector('meta[name=csrf-token]').content
			//document.querySelector('meta[name="csrf-token"]').getAttribute('content')
			//document.head.querySelector('meta[name="csrf-token"]') ? document.head.querySelector('meta[name="csrf-token"]').content : '',
			headers: {
				//'X-CSRF-TOKEN': jJS._token
				//'X-CSRF-TOKEN': 'eyJpdiI6Im42UDdiRGg1anZqSGFpNkhXK0VxSFE9PSIsInZhbHVlIjoiVm1Ncm45YnFiaVQwV3VxNmJlbVRQdS9mZk1YTmZrcTlXZmlRclZXZWk2Q2Zwc1hXQm9wa2NnZ3QzczNic2xnSXliQ1FlcytXcDgyYTg5NUJtR2hMUlFUTzhhTGErbFpZSGMzdENRMGliWWlMTUFibmhxclZpdm42aFJxaXNDbFoiLCJtYWMiOiJhZTExYWU4ZGVhMTg4YmRlMjc0MzQxZjhiODE1Y2FmZTg2ZDAxNDBmODI1MWUzMWJjNGQ5OWNiMDMxNzhkYTY5IiwidGFnIjoiIn0='
			},
			onError (error) {
			  if (error.originalRequest) {
				if (window.confirm(`Failed because: ${error}\nDo you want to retry?`)) {
				  if (typeof(jJS.uploads[n]) != "undefined" && jJS.uploads[n] !== null) {
					  jJS.uploads[n].start()
					  jJS.arrUploadIsRunning[n] = true
					  return
				  }
				}
			  } else {
				window.alert(`Failed because: ${error}`)
			  }

			  reset()
			},
			onProgress (bytesUploaded, bytesTotal) {
			  const percentage = ((bytesUploaded / bytesTotal) * 100).toFixed(2)
			  //progressFileName.innerHTML = file.name;
			  jJS.arrProgressBar[n].style.width = `${percentage}%`
			  jJS.arrProgressBar[n].innerHTML = `${percentage}%`;
			  console.log(bytesUploaded, bytesTotal, `${percentage}%`)
			},
			onSuccess () {
				var p = "cmd=add&f="+file.name+"&sid="+jJS._token;
				//alert(p);
				jJS.a(jJS.tusurl, p);
				
				jJS.arrUploadIsRunning[n] = false;
				jJS.UploadSuccesses[n] = true;
				/*jJS.progressTable.removeChild(jJS.trNames[n]);
				jJS.progressTable.removeChild(jJS.trProgresses[n]);
				*/
			},
		  }
			this.UploadSuccesses[n] = false;
			this.uploads[n] = new tus.Upload(file, options)
			console.log(tus);
			this.uploads[n].findPreviousUploads().then((previousUploads) => {
			this.askToResumeUpload(previousUploads, this.uploads[n])

			this.uploads[n].start()
			this.arrUploadIsRunning[n] = true;
		  })
		}
	}
	
	, askToResumeUpload: function (previousUploads, currentUpload) {
		if (previousUploads.length === 0) return

		let text = 'You tried to upload this file previously at these times:\n\n'
		previousUploads.forEach((previousUpload, index) => {
		text += `[${index}] ${previousUpload.creationTime}\n`
		})
		text += '\nEnter the corresponding number to resume an upload or press Cancel to start a new upload'

		//const answer = prompt(text)
		const index = 0;//parseInt(answer, 10)

		if (!Number.isNaN(index) && previousUploads[index]) {
			currentUpload.resumeFromPreviousUpload(previousUploads[index])
		}
	}
	
	, a: function (url, pst) {
		if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		  var xhr = new XMLHttpRequest();
		}
		else {// code for IE6, IE5
		  var xhr = new ActiveXObject("Microsoft.XMLHTTP");      
		}
		
		xhr.ontimeout = (e) => {
		  
		};
			  
		  xhr.onload = () => {			
			if (xhr.readyState === 4) {
			  if (xhr.status === 200) {
				this.ares(xhr.responseURL, xhr.responseText);
			  }
			}			
		  };
		  
		  xhr.onerror = (e) => {
			alert(xhr.statusText);
		  };
		  
		
	
		xhr.open("POST", url, true);
		xhr.setRequestHeader('Pragma', 'no-cache');
		xhr.setRequestHeader('Cache-Control', 'no-store, no-cache, must-revalidate, max-age=0');
		xhr.setRequestHeader("If-Modified-Since", "Sat, 1 Jan 2000 00:00:00 GMT");
		xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		xhr.timeout = this.atimeout;
		xhr.send(pst);
	}
	
	, ares: function(rurl, rtxt) {
		if(rtxt!='')
			alert(rurl+" "+rtxt);
	}
	
	
}
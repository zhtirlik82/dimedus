import './assets/main.css'

import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import axios from 'axios'
import VueCookies from 'vue-cookies';

axios.defaults.baseURL = 'http://194.110.54.178:8001/api/';
//axios.defaults.withCredentials = true
const app = createApp(App)
app.use(VueCookies);
app.use(router)
router.app = app
app.mount('#app')

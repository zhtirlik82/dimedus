<?php  
	session_start();
	error_reporting(E_ALL);
	ini_set('display_errors','On');
	define("TUSDIR", '/var/www/html/DimedusLaravel/tus/files/');
	define("VENDORDIR", '/var/www/html/DimedusLaravel/vendor/');
	$sid = 'sessionid';//session_id()
	$tusdir = TUSDIR.$sid;
	require_once VENDORDIR.'autoload.php';
	//echo $tusdir;
	if(!is_dir($tusdir)) {
	  mkdir($tusdir);
	  chmod($tusdir, 777);
	}

	//$server   = new \TusPhp\Tus\Server('redis');
	$server   = new \TusPhp\Tus\Server('file');
	$server->setUploadDir($tusdir);
	$response = $server->serve();
	$response->send();
	exit(0);  
?>
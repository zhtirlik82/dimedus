<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Controller;
use App\Models\DFiles;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


Route::get('/getfile/{id}', function ($id) {
	$dFiles = DFiles::find($id);
	$fn = $dFiles->dfilename;
	$file = $dFiles->dfileff;
	//print_r($file);
	header("Access-Control-Allow-Origin: *");
	header("Access-Control-Allow-Headers: *");
	header('Content-Description: File Transfer');
	header('Content-Type: application/octet-stream');
	header('Content-Disposition: attachment; filename='.$fn);
	header('Content-Transfer-Encoding: binary');
	header('Content-Length: ' . filesize($dFiles->dfilesz));
	header('Expires: 0');
	header('Cache-Control: must-revalidate');
	header('Pragma: public');
	
	ob_clean();
	flush();
	readfile($file);
	return;
});

Route::get('/getfilem/{id}', function ($id) {
	$dFiles = DFiles::find($id);
	$fn = $dFiles->dfilename;
	$file = $dFiles->dfilemm;
	//print_r($file);
	header("Access-Control-Allow-Origin: *");
	header("Access-Control-Allow-Headers: *");
	header('Content-Description: File Transfer');
	header('Content-Type: application/octet-stream');
	header('Content-Disposition: attachment; filename='.$fn);
	header('Content-Transfer-Encoding: binary');
	header('Content-Length: ' . filesize($dFiles->dfilesz));
	header('Expires: 0');
	header('Cache-Control: must-revalidate');
	header('Pragma: public');
	
	ob_clean();
	flush();
	readfile($file);
	return;
});

Route::get('/', function () {
    return view('welcome');
});

//Route::match(array('GET', 'POST', 'PUT', 'PATCH'), '/tus', function (Request $request) {
//Route::get('/tus', function (Request $request) {
Route::middleware([\App\Http\Middleware\Cors::class])->match(array('GET', 'POST', 'PUT', 'PATCH'), '/tus', function (Request $request) {
//Route::middleware([\App\Http\Middleware\Cors::class])->get('/tus', function (Request $request) {
    $TUSDIR = '/var/www/html/DimedusLaravel/tus/files/';
	//$VENDORDIR = '/var/www/html/DimedusLaravel/vendor/';
	$sid = 'sessionid';//session_id()
	$tusdir = $TUSDIR.$sid.'/';
	//require_once $VENDORDIR.'autoload.php';
	//echo $tusdir;
	if(!is_dir($tusdir)) {
	  mkdir($tusdir);
	  //chmod($tusdir, 777);
	}

	//$server   = new \TusPhp\Tus\Server('redis');
	$server   = new \TusPhp\Tus\Server('file');
	$server->setUploadDir($tusdir);
	$response = $server->serve();
	$response->send();
	//exit(0);
	return;
});
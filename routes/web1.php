<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Controller;
use App\Models\DFiles;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
/*
Route::get('/tus', function () {
	$r = 'tus';
	header('Access-Control-Allow-Origin: *');
	define("TUSDIR", '/var/www/html/DimedusLaravel/tus/files/');
	define("VENDORDIR", '/var/www/html/DimedusLaravel/vendor/');
	$sid = 'sessionid';//session_id()
	$tusdir = TUSDIR.$sid.'/';
	require_once VENDORDIR.'autoload.php';
	//echo $tusdir;
	if(!is_dir($tusdir)) {
	  mkdir($tusdir);
	  //chmod($tusdir, 777);
	}

	//$server   = new \TusPhp\Tus\Server('redis');
	$server   = new \TusPhp\Tus\Server('file');
	$server->setUploadDir($tusdir);
	$response = $server->serve();
	$response->send();
    //return print_r($_GET, true);
	return response()->json([
            'success' => true,
            'message' => 'List data post',
            'data' => ''
        ], 200);
});
*/
/*
Route::middleware(['cors'])->group(function () {
	//Route::get('/tus', 'Controller@hogehoge');
	
    Route::get('/tus', function () {
		$r = '';
		header('Access-Control-Allow-Origin: *');
		define("TUSDIR", '/var/www/html/DimedusLaravel/tus/files/');
		define("VENDORDIR", '/var/www/html/DimedusLaravel/vendor/');
		$sid = 'sessionid';//session_id()
		$tusdir = TUSDIR.$sid.'/';
		require_once VENDORDIR.'autoload.php';
		//echo $tusdir;
		if(!is_dir($tusdir)) {
		  mkdir($tusdir);
		  //chmod($tusdir, 777);
		}

		//$server   = new \TusPhp\Tus\Server('redis');
		$server   = new \TusPhp\Tus\Server('file');
		$server->setUploadDir($tusdir);
		$response = $server->serve();
		$response->send();
		//return print_r($_GET, true);
		return response()->json([
            'success' => true,
            'message' => 'List data post',
            'data' => ''
        ], 200);
		//return $r;
	});
});
*/
Route::middleware(['cors'])->group(function () {
	Route::get('/tus2', function () {
		
		$TUSDIR = '/var/www/html/DimedusLaravel/tus/files/';
		$VENDORDIR = '/var/www/html/DimedusLaravel/vendor/';
		$sid = 'sessionid';//session_id()
		$tusdir = $TUSDIR.$sid.'/';
		//require_once $VENDORDIR.'autoload.php';
		//echo $tusdir;
		if(!is_dir($tusdir)) {
		  mkdir($tusdir);
		  //chmod($tusdir, 777);
		}

		//$server   = new \TusPhp\Tus\Server('redis');
		$server   = new \TusPhp\Tus\Server('file');
		$server->setUploadDir($tusdir);
		$response1 = $server->serve();
		//$response1->send();
		//exit(0);
		
	});
	
	Route::post('/tus2', function () {
		
	});
});




Route::get('/tus1',  function  (Request $request)  {
//Route::middleware([\App\Http\Middleware\Cors::class])->get('/tus', function (Request $request) {	
	$TUSDIR = '/var/www/html/DimedusLaravel/tus/files/';
	$VENDORDIR = '/var/www/html/DimedusLaravel/vendor/';
	$sid = 'sessionid';//session_id()
	$tusdir = $TUSDIR.$sid.'/';
	require_once $VENDORDIR.'autoload.php';
	//echo $tusdir;
	if(!is_dir($tusdir)) {
	  mkdir($tusdir);
	  //chmod($tusdir, 777);
	}

	//$server   = new \TusPhp\Tus\Server('redis');
	$server   = new \TusPhp\Tus\Server('file');
	$server->setUploadDir($tusdir);
	$response = $server->serve();
	$response->send();
    //return 'ffff';
	return response()->json([
            'success' => true,
            'message' => 'List data post',
            'data' => ''
        ], 200);
});

Route::post('/tus3', function (Request $request) {
    return view('welcome');
});

Route::get('/tus3', function (Request $request) {
    return view('welcome');
});

Route::middleware([\App\Http\Middleware\Cors::class])->get('/tus', function (Request $request) {
    return '';
});
Route::middleware([\App\Http\Middleware\Cors::class])->post('/tus', function (Request $request) {
    $r = '';
	$TUSDIR = '/var/www/html/DimedusLaravel/tus/files/';
	$VENDORDIR = '/var/www/html/DimedusLaravel/vendor/';
	$sid = 'sessionid';//session_id()
	$tusdir = $TUSDIR.$sid.'/';
	require_once $VENDORDIR.'autoload.php';
	//echo $tusdir;
	if(!is_dir($tusdir)) {
	  mkdir($tusdir);
	  //chmod($tusdir, 777);
	}

	//$server   = new \TusPhp\Tus\Server('redis');
	$server   = new \TusPhp\Tus\Server('file');
	$server->setUploadDir($tusdir);
	$response = $server->serve();
	$response->send();
    //return print_r($_GET, true);
	exit(0);
	return $r;
});

Route::get('/', function () {
    return view('welcome');
});
/*
Route::middleware([\App\Http\Middleware\Cors::class])->get('/files/{id}', function (Request $request) {
	$TUSDIR = '/var/www/html/DimedusLaravel/tus/files/';
	$VENDORDIR = '/var/www/html/DimedusLaravel/vendor/';
	$sid = 'sessionid';//session_id()
	$tusdir = $TUSDIR.$sid.'/';
	require_once $VENDORDIR.'autoload.php';
	//echo $tusdir;
	if(!is_dir($tusdir)) {
	  mkdir($tusdir);
	  //chmod($tusdir, 777);
	}

	//$server   = new \TusPhp\Tus\Server('redis');
	$server   = new \TusPhp\Tus\Server('file');
	$server->setUploadDir($tusdir);
	$response = $server->serve();
	$response->send();
	return '';
});

Route::middleware([\App\Http\Middleware\Cors::class])->post('/files/{id}', function (Request $request) {
	$TUSDIR = '/var/www/html/DimedusLaravel/tus/files/';
	$VENDORDIR = '/var/www/html/DimedusLaravel/vendor/';
	$sid = 'sessionid';//session_id()
	$tusdir = $TUSDIR.$sid.'/';
	require_once $VENDORDIR.'autoload.php';
	//echo $tusdir;
	if(!is_dir($tusdir)) {
	  mkdir($tusdir);
	  //chmod($tusdir, 777);
	}

	//$server   = new \TusPhp\Tus\Server('redis');
	$server   = new \TusPhp\Tus\Server('file');
	$server->setUploadDir($tusdir);
	$response = $server->serve();
	$response->send();
	return '';
});

Route::middleware([\App\Http\Middleware\Cors::class])->patch('/files1/{id}', function (Request $request) {
	$TUSDIR = '/var/www/html/DimedusLaravel/tus/files/';
	$VENDORDIR = '/var/www/html/DimedusLaravel/vendor/';
	$sid = 'sessionid';//session_id()
	$tusdir = $TUSDIR.$sid.'/';
	require_once $VENDORDIR.'autoload.php';
	//echo $tusdir;
	if(!is_dir($tusdir)) {
	  mkdir($tusdir);
	  //chmod($tusdir, 777);
	}

	//$server   = new \TusPhp\Tus\Server('redis');
	$server   = new \TusPhp\Tus\Server('file');
	$server->setUploadDir($tusdir);
	$response = $server->serve();
	$response->send();
	return null;
});
*/



/*
Route::get('/files/{id}', function (Request $request) {
	$TUSDIR = '/var/www/html/DimedusLaravel/tus/files/';
	$VENDORDIR = '/var/www/html/DimedusLaravel/vendor/';
	$sid = 'sessionid';//session_id()
	$tusdir = $TUSDIR.$sid.'/';
	require_once $VENDORDIR.'autoload.php';
	//echo $tusdir;
	if(!is_dir($tusdir)) {
	  mkdir($tusdir);
	  //chmod($tusdir, 777);
	}

	//$server   = new \TusPhp\Tus\Server('redis');
	$server   = new \TusPhp\Tus\Server('file');
	$server->setUploadDir($tusdir);
	$response = $server->serve();
	$response->send();
    return '';
});*/

//Route::permanentRedirect('/files/{id}', '/api/files/{id}');
/*
//Route::patch('/files/{id}', function (Request $request) {
Route::middleware([\App\Http\Middleware\Cors::class])->patch('/files/{id}', function (Request $request) {
	$TUSDIR = '/var/www/html/DimedusLaravel/tus/files/';
	$VENDORDIR = '/var/www/html/DimedusLaravel/vendor/';
	$sid = 'sessionid';//session_id()
	$tusdir = $TUSDIR.$sid.'/';
	require_once $VENDORDIR.'autoload.php';
	//echo $tusdir;
	if(!is_dir($tusdir)) {
	  mkdir($tusdir);
	  //chmod($tusdir, 777);
	}

	//$server   = new \TusPhp\Tus\Server('redis');
	$server   = new \TusPhp\Tus\Server('file');
	$server->setUploadDir($tusdir);
	$response = $server->serve();
	$response->send();
    return '';
});
*/
Route::get('/files/{id}', function (Request $request) {
//Route::match(array('GET', 'POST', 'PUT', 'PATCH'), '/files/{id}', function (Request $request) {
    $TUSDIR = '/var/www/html/DimedusLaravel/tus/files/';
	//$VENDORDIR = '/var/www/html/DimedusLaravel/vendor/';
	$sid = 'sessionid';//session_id()
	$tusdir = $TUSDIR.$sid.'/';
	//require_once $VENDORDIR.'autoload.php';
	//echo $tusdir;
	if(!is_dir($tusdir)) {
	  mkdir($tusdir);
	  //chmod($tusdir, 777);
	}

	//$server   = new \TusPhp\Tus\Server('redis');
	$server   = new \TusPhp\Tus\Server('file');
	$server->setUploadDir($tusdir);
	$response = $server->serve();
	$response->send();
	exit(0); 
});
/*
Route::patch('/files/{id}', function (Request $request) {
});
Route::put('/files/{id}', function (Request $request) {
});
Route::post('/files/{id}', function (Request $request) {
});*/

Route::get('/getfile/{id}', function ($id) {
	$TUSDIR = '/var/www/html/DimedusLaravel/tus/files/';
	$sid = 'sessionid/';//session_id()
	$tusdir = $TUSDIR.$sid;
	$dFiles = DFiles::find($id);
	$fn = $dFiles->dfilename;
	$file = $tusdir.$dFiles->dfilemm;
	print_r($file);
	/*
	header('Content-Description: File Transfer');
	header('Content-Type: application/octet-stream');
	header('Content-Disposition: attachment; filename='.$fn);
	header('Content-Transfer-Encoding: binary');
	header('Expires: 0');
	header('Cache-Control: must-revalidate');
	header('Pragma: public');
	header('Content-Length: ' . filesize($file));
	ob_clean();
	flush();
	readfile($file);*/
});

Route::get('/getfilem/{id}', function ($id) {
	$TUSDIR = '/var/www/html/DimedusLaravel/tus/files/';
	$dFiles = DFiles::find($id);
	
	
	$fn = $dFiles->dfilename;
	$file = $TUSDIR.$dFiles->dfilemm;
	print_r($file);
	/*header('Content-Description: File Transfer');
	header('Content-Type: application/octet-stream');
	header('Content-Disposition: attachment; filename='.$fn);
	header('Content-Transfer-Encoding: binary');
	header('Expires: 0');
	header('Cache-Control: must-revalidate');
	header('Pragma: public');
	header('Content-Length: ' . filesize($file));
	ob_clean();
	flush();
	readfile($file);*/
});
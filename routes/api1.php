<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\DFilesController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/
/*
Route::get('/tus',  function  (Request $request)  {
	define("TUSDIR", '/var/www/html/DimedusLaravel/tus/files/');
	define("VENDORDIR", '/var/www/html/DimedusLaravel/vendor/');
	$sid = 'sessionid';//session_id()
	$tusdir = TUSDIR.$sid.'/';
	require_once VENDORDIR.'autoload.php';
	//echo $tusdir;
	if(!is_dir($tusdir)) {
	  mkdir($tusdir);
	  //chmod($tusdir, 777);
	}

	//$server   = new \TusPhp\Tus\Server('redis');
	$server   = new \TusPhp\Tus\Server('file');
	$server->setUploadDir($tusdir);
	$response1 = $server->serve();
	$response1->send();
	//{"success":true,"message":"List data post","data":[{"id":5,"dfilename":"1","dfilesizemb":1,"dfileext":"11112","created_at":"2023-08-04T13:13:21.000000Z","updated_at":"2023-08-04T13:22:12.000000Z"},{"id":4,"dfilename":"1","dfilesizemb":1,"dfileext":"1","created_at":"2023-08-04T12:35:48.000000Z","updated_at":"2023-08-04T12:35:48.000000Z"}]}
   return response()->json(["success"=>true,"message"=>"List data post","data"=>'']);
});
Route::get('tus1', function () {
	$r = '';
	//header('Access-Control-Allow-Origin: *');
	define("TUSDIR", '/var/www/html/DimedusLaravel/tus/files/');
	define("VENDORDIR", '/var/www/html/DimedusLaravel/vendor/');
	$sid = 'sessionid';//session_id()
	$tusdir = TUSDIR.$sid.'/';
	require_once VENDORDIR.'autoload.php';
	//echo $tusdir;
	if(!is_dir($tusdir)) {
	  mkdir($tusdir);
	  //chmod($tusdir, 777);
	}

	//$server   = new \TusPhp\Tus\Server('redis');
	$server   = new \TusPhp\Tus\Server('file');
	$server->setUploadDir($tusdir);
	$response = $server->serve();
	$response->send();
    //return print_r($_GET, true);
	return $r;
});*/
Route::apiResource('dfiles',DFilesController::class);
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware([\App\Http\Middleware\Cors::class])->get('/tt', function (Request $request) {
    return '';
});
Route::middleware([\App\Http\Middleware\Cors::class])->post('/tt', function (Request $request) {
    $r = '';
	$TUSDIR = '/var/www/html/DimedusLaravel/tus/files/';
	$VENDORDIR = '/var/www/html/DimedusLaravel/vendor/';
	$sid = 'sessionid';//session_id()
	$tusdir = $TUSDIR.$sid.'/';
	require_once $VENDORDIR.'autoload.php';
	//echo $tusdir;
	if(!is_dir($tusdir)) {
	  mkdir($tusdir);
	  //chmod($tusdir, 777);
	}

	//$server   = new \TusPhp\Tus\Server('redis');
	$server   = new \TusPhp\Tus\Server('file');
	$server->setUploadDir($tusdir);
	$response = $server->serve();
	$response->send();
    //return print_r($_GET, true);
	exit(0);
	return $r;
});
